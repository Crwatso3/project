class Report < ActiveRecord::Base
  has_many :quotes
  before_save :calc_tgr, :calc_st_total, :calc_net_profit



  def calc_tgr
    self.tgr = Quote.where(:sold => true).sum("total") - Car.where(:sold => true).sum("whs_price") + Loan.sum("interest_sum")
  end

  def calc_st_total
    self.sales_tax_total= Quote.where(:sold => true).sum("sales_tax")
  end

  def calc_net_profit
    self.net_profit= self.calc_tgr - self.calc_st_total + Loan.sum("interest_sum")
  end



end
