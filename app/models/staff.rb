class Staff < ActiveRecord::Base
  has_many :quotes
  def name
    "#{first_name}  #{last_name}"
  end

  validates :first_name, :last_name, presence: true
  validates :first_name, :last_name, format: {with: /\A[a-zA-Z]+\z/}

end
