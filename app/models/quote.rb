class Quote < ActiveRecord::Base

  has_one :loan
  belongs_to :car
  belongs_to :staff
  belongs_to :customer
  belongs_to :report

  after_save :marksold
  before_save :grab_price, :calc_sales_tax, :calc_total


  def marksold
    if self.sold == true
      Car.update(self.car_id, :sold => true)
    end

  end

  def grab_price
    self.price= self.car.mu_price
  end

  def calc_sales_tax
    self.sales_tax= self.price * 0.043
  end

  def calc_total
    self.total= self.price + self.sales_tax
  end


  def cuname
    "#{customer.first_name} - #{customer.last_name} - #{car.model} - #{car.color} - #{car.vin}"
  end



  def quotetype
    "#{customer.cname} - #{car.cartype}"
  end

end
