class Customer < ActiveRecord::Base
  has_many :quotes

  def cname
    "#{first_name}  #{last_name}"
  end

  def self.search(search)
    if search
      where('first_name LIKE ? OR last_name LIKE ?',
            "%#{search}%", "%#{search}%")
    else
      self.all
    end
  end

  validates :first_name, :last_name, :phone, :email, presence: true
  validates :first_name, :last_name, format: {with: /\A[a-zA-Z]+\z/}
  validates :phone, numericality: true, length: { is: 10}
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

end
