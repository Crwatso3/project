class Loan < ActiveRecord::Base
  belongs_to :quote
  before_save :interest_total

  def self.calc_amortization_table(initial_amount = 0, interest = 0, years = 0, down_payment = 0)
    Rails.logger.debug "Inputs: #{initial_amount} #{interest} #{years} #{down_payment}"
    calculation = []
    num_of_months = (years * 12)-1
    interest_per_year = (interest/100.0)/12.0
    init_dp = BigDecimal(initial_amount) - BigDecimal(down_payment)
    amt_per_payment = (BigDecimal(init_dp)*((BigDecimal(interest)/100.0)/12.0))/(1.0-(1.0+((BigDecimal(interest)/100.0)/12.0))**(-1.0*(BigDecimal(years)*12.0)))
    count = 0
    interest = BigDecimal(interest)
    (0..num_of_months).each do
       calculation << count+= 1
       calculation << '$%.2f' % amt_per_payment
       interest_per_month = init_dp * interest_per_year
       calculation << '$%.2f' % interest_per_month
      principal_per_month = BigDecimal(amt_per_payment) - BigDecimal(interest_per_month)
      calculation << '$%.2f' % principal_per_month
      balance = init_dp -= principal_per_month
      if balance < 0
        balance = 0
      end
      calculation << '$%.2f' % balance.round(2)
    end
    return calculation
    end

  def calc_amortization_table
    return self.class.calc_amortization_table(self.initial_amount,self.interest_rate,self.years,self.down_payment)
  end

  def interest_total
    self.interest_sum = (BigDecimal(self.initial_amount-self.down_payment)*((BigDecimal(self.interest_rate)/100.0)/12.0))/(1.0-(1.0+((BigDecimal(self.interest_rate)/100.0)/12.0))**(-1.0*(BigDecimal(self.years)*12.0)))*((self.years * 12)-1) - (self.initial_amount-self.down_payment)
  end

  validates :interest_rate, presence: true, numericality: true

end


