class Car < ActiveRecord::Base

  has_many :quotes
  before_save :calc_mu_price

  def cartype
    "#{model} - #{year} - #{color} - #{vin} "
  end

  def self.search(search)
    if search
      where('model LIKE ? OR Vin LIKE ? OR color LIKE ?',
            "%#{search}%", "%#{search}%", "%#{search}%")
    else
      self.all
    end
  end

  def calc_mu_price
      self.mu_price = self.whs_price + (self.whs_price * 0.1)
  end

  validates :model, :color, :year, :vin, :whs_price, presence: true
  validates :year, length: { is: 4}
  validates :vin, length: {is: 17}
  validates :color, format: {with: /\A[a-zA-Z]+\z/}
  validates :year, :whs_price, numericality: true
  validates :model, :color, length: {maximum: 30}

end
