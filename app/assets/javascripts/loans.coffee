# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


updateAmount = ->
  selection_id = $('#loan_quote_id').val()
  $.getJSON '/quotes/' + selection_id + '/total', {},(json, response) ->
    amount = json['total']
    amount2 = parseFloat(amount)
    amount3 = amount2.toFixed(2)
    $('#loan_initial_amount').val(amount3)

build_table = ->
  time = $("#loan_years").val()
  initial_amount = $("#loan_initial_amount").val()
  interest_rate = $("#loan_interest_rate").val()
  down_payment = $("#loan_down_payment").val()
  $.get " /loans/amortization", {initial_amount: initial_amount, years: time, interest: interest_rate, down_payment: down_payment}

$(document).on 'page:change', ->
  if $('#loan_quote_id').length > 0
    updateAmount()
  $('#loan_quote_id').change -> updateAmount()
  $('#loan_initial_amount,#loan_interest,#loan_years,#down_payment').bind 'change keyup mouseup mousewheel', ->
    build_table()
  build_table()

