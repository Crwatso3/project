# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updatePrice = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/price', {},(json, response) ->
    price1 = json['mu_price']
    price2 = parseFloat(price1)
    price3 = price2.toFixed(2)
    $('#quote_price').val(price3)
    st = price1 * .043
    formatst = st.toFixed(2)
    $('#quote_sales_tax').val(formatst)
    total = parseFloat(price1) + parseFloat(st)
    formattotal = total.toFixed(2)
    $('#quote_total').val(formattotal)

$(document).on 'page:change', ->
  if $('#quote_car_id').length > 0
    updatePrice()
  $('#quote_car_id').change -> updatePrice()

