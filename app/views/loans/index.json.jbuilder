json.array!(@loans) do |loan|
  json.extract! loan, :id, :quote_id, :initial_amount, :years, :down_payment, :interest_rate, :interest_sum
  json.url loan_url(loan, format: :json)
end
