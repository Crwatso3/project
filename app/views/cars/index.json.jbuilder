json.array!(@cars) do |car|
  json.extract! car, :id, :model, :color, :vin, :year, :whs_price, :mu_price, :sold
  json.url car_url(car, format: :json)
end
