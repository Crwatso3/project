json.array!(@quotes) do |quote|
  json.extract! quote, :id, :staff_id, :customer_id, :car_id, :price, :sales_tax, :total, :sold
  json.url quote_url(quote, format: :json)
end
