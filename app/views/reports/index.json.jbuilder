json.array!(@reports) do |report|
  json.extract! report, :id, :tgr, :net_profit, :sales_tax_total, :date
  json.url report_url(report, format: :json)
end
