json.array!(@staffs) do |staff|
  json.extract! staff, :id, :first_name, :last_name, :position
  json.url staff_url(staff, format: :json)
end
