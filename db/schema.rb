# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150428020428) do

  create_table "cars", force: :cascade do |t|
    t.string   "model"
    t.string   "color"
    t.integer  "year"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "vin"
    t.decimal  "whs_price"
    t.decimal  "mu_price"
    t.boolean  "sold",       default: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "email"
    t.string   "cust_status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "loans", force: :cascade do |t|
    t.integer  "quote_id"
    t.decimal  "initial_amount"
    t.integer  "years"
    t.decimal  "down_payment"
    t.decimal  "interest_rate"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.decimal  "interest_sum"
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "staff_id"
    t.integer  "car_id"
    t.decimal  "price"
    t.decimal  "sales_tax"
    t.decimal  "total"
    t.boolean  "sold"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "customer_id"
  end

  create_table "reports", force: :cascade do |t|
    t.decimal  "net_profit"
    t.decimal  "sales_tax_total"
    t.string   "date"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.decimal  "tgr"
  end

  create_table "staffs", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
