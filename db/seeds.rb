# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

    cars = Car.create([{ model: 'Honda Accord', color: 'Yellow', vin: '8H54JG69ERV5L3125', year: '2012', whs_price: 17000.00}])
    cars = Car.create([{ model: 'Dodge Ram 1500', color: 'Green', vin: '3J97QU54POX8O8467', year: '2003', whs_price: 6000.00}])
    cars = Car.create([{ model: 'Honda Civic', color: 'Blue', vin: '1Z31EV75SED8M5117', year: '2005', whs_price: 9000.00}])
    cars = Car.create([{ model: 'Lexus GS', color: 'Black', vin: '5K27BY16KKS7I8861', year: '2008', whs_price: 20000.00}])
    cars = Car.create([{ model: 'Toyota Tundra', color: 'Orange', vin: '3P69IO87SAQ2I6634', year: '2002', whs_price: 5000.00}])
    cars = Car.create([{ model: 'Mustang', color: 'Silver', vin: '5O92FW34FAY8V1671', year: '2015', whs_price: 30000.00}])
    cars = Car.create([{ model: 'Corvette', color: 'White', vin: '2D18VR54BUI6A8366', year: '2013', whs_price: 25000.00}])
    cars = Car.create([{ model: 'Toyota Prius', color: 'Red', vin: '8Y38MO77POC1E4643', year: '2010', whs_price: 12000.00}])
    cars = Car.create([{ model: 'Nissan Altima', color: 'Grey', vin: '9L10KI90VTK6C2338', year: '2008', whs_price: 12500.00}])
    cars = Car.create([{ model: 'Dodge Caravan', color: 'Gold', vin: '4Y60EH49TIM7M4920', year: '2000', whs_price: 4500.00}])
