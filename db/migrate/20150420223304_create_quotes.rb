class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :staff_id
      t.integer :cust_id
      t.integer :car_id
      t.decimal :price
      t.decimal :sales_tax
      t.decimal :total
      t.boolean :sold

      t.timestamps null: false
    end
  end
end
