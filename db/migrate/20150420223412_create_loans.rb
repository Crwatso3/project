class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :quote_id
      t.decimal :initial_amount
      t.integer :years
      t.decimal :down_payment
      t.decimal :interest_rate
      t.decimal :payment_amt

      t.timestamps null: false
    end
  end
end
