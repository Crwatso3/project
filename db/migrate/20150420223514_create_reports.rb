class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.decimal :TGR
      t.decimal :net_profit
      t.decimal :sales_tax_total
      t.string :date

      t.timestamps null: false
    end
  end
end
