class ChangeColumnName < ActiveRecord::Migration
  def change
    remove_column :cars, :Vin , :integer
    add_column :cars, :vin, :string
  end
end
