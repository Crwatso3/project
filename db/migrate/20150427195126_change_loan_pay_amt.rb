class ChangeLoanPayAmt < ActiveRecord::Migration
  def change
    remove_column :loans, :payment_amt, :decimal
  end
end
