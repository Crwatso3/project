class ChangeSold < ActiveRecord::Migration
  def change
    remove_column :cars, :sold , :boolean
    add_column :cars, :sold, :boolean, :default => false
  end
end
