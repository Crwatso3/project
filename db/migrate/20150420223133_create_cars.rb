class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :model
      t.string :color
      t.integer :Vin
      t.integer :year
      t.decimal :WHS_Price
      t.decimal :MU_Price
      t.boolean :sold

      t.timestamps null: false
    end
  end
end
