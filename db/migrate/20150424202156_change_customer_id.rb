class ChangeCustomerId < ActiveRecord::Migration
  def change
    remove_column :quotes, :cust_id , :integer
    add_column :customers, :cust_id, :integer
  end
end
