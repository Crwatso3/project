class AddLoanInterestSum < ActiveRecord::Migration
  def change
    add_column :loans, :interest_sum, :decimal
  end
end
